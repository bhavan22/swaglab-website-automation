package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OverviewPage {
	WebDriver driver;
	private WebElement finishButtonElement;
	private WebElement sideBarMenuElement;
	private WebElement logoutButtonElement;

	public OverviewPage(WebDriver driver) {
		this.driver = driver;
		this.finishButtonElement = driver.findElement(By.cssSelector("button[id='finish']"));
		this.sideBarMenuElement = driver.findElement(By.cssSelector("button[id='react-burger-menu-btn']"));

	}

	public WebElement getFinishButtonElement() {
		return finishButtonElement;
	}

	public void clickOnFinishButton() {
		finishButtonElement.click();
	}

	public void clickOnSideBarAndLogout() {
		sideBarMenuElement.click();
		logoutButtonElement.click();
	}
}

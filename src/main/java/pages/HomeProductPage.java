package pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomeProductPage {
	WebDriver driver;
	private WebElement loginConformationelElement;
	private WebElement cartButtonElement;

	public HomeProductPage(WebDriver driver) {
		this.driver = driver;
		this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
		this.loginConformationelElement = driver.findElement(By.cssSelector("span[class='title']"));
		this.cartButtonElement = driver.findElement(By.cssSelector("a[class='shopping_cart_link']"));
	}

	public String getLoginConformationMessage() {
		String conformationMessageString = "";
		conformationMessageString = loginConformationelElement.getText();
		return conformationMessageString;
	}

	public void selectItemAndAddtoCart(String item) {
		List<WebElement> items = driver.findElements(By.cssSelector("div[class='inventory_item']"));
		for (WebElement i : items) {
			String itemNameString = i.findElement(By.cssSelector("div[class='inventory_item_name']")).getText();

			if (itemNameString.contains(item)) {
				i.findElement(By.cssSelector("button[class='btn btn_primary btn_small btn_inventory']")).click();
			}
		}
	}

	public void clickOnCartButton() {
		cartButtonElement.click();
	}

}

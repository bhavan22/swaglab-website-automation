package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CheckOutPage {
	WebDriver driver;
	private WebElement firstNameElement;
	private WebElement lastNamElement;
	private WebElement zipCodeElement;
	private WebElement continueButtonElement;
	private WebElement cancelButtonEeElement;

	public CheckOutPage(WebDriver driver) {
		this.driver = driver;
		this.firstNameElement = driver.findElement(By.cssSelector("input[id='first-name']"));
		this.lastNamElement = driver.findElement(By.cssSelector("input[id='last-name']"));
		this.zipCodeElement = driver.findElement(By.cssSelector("input[id='postal-code']"));
		this.continueButtonElement = driver.findElement(By.cssSelector("input[id='continue']"));
		this.cancelButtonEeElement = driver.findElement(By.cssSelector("button[id='cancel']"));
	}

	public WebElement getFirstNameElement() {
		return firstNameElement;
	}

	public WebElement getLastNamElement() {
		return lastNamElement;
	}

	public WebElement getZipCodeElement() {
		return zipCodeElement;
	}

	public WebElement getContinueButtonElement() {
		return continueButtonElement;
	}

	public WebElement getCancelButtonEeElement() {
		return cancelButtonEeElement;
	}

	public void fillTheCheckOutDetails(String fname, String lname, String zipcode) {
		firstNameElement.sendKeys(fname);
		lastNamElement.sendKeys(lname);
		zipCodeElement.sendKeys(zipcode);
	}

	public void clickOnContinue() {
		continueButtonElement.click();
	}

}

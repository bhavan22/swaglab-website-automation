package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YourCartPage {
	WebDriver driver;
	private WebElement checkOutButtonElement;
	private WebElement continueShopingButtonElement;

	public YourCartPage(WebDriver driver) {
		this.driver = driver;
		this.checkOutButtonElement = driver.findElement(By.cssSelector("button[id='checkout']"));
		this.continueShopingButtonElement = driver.findElement(By.cssSelector("button[id='continue-shopping']"));

	}

	public WebElement getCheckOutButtonElement() {
		return checkOutButtonElement;
	}

	public WebElement getContinueShopingButtonElement() {
		return continueShopingButtonElement;
	}

	public void proceedToCheckout() {
		checkOutButtonElement.click();
	}
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	WebDriver driver;
	private WebElement userNamElement;
	private WebElement passwordElement;
	private WebElement loginButtonElement;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		this.userNamElement = driver.findElement(By.cssSelector("input[id='user-name']"));
		this.passwordElement = driver.findElement(By.cssSelector("input[id='password']"));
		this.loginButtonElement = driver.findElement(By.cssSelector("input[id='login-button']"));
	}

	public WebElement getUserNamElement() {
		return userNamElement;
	}

	public WebElement getPasswordElement() {
		return passwordElement;
	}

	public WebElement getLoginButtonElement() {
		return loginButtonElement;
	}

	public void inputUsernamePassword(String userName, String password) {
		userNamElement.sendKeys(userName);
		passwordElement.sendKeys(password);
	}

	public void dologin() {
		loginButtonElement.click();
	}

	public String getloginErrorMessage() {
		String messageString = "";
		messageString = driver.findElement(By.cssSelector("h3[data-test='error']")).getText();
		return messageString;

	}

}

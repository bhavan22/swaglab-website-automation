package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.HomeProductPage;
import pages.LoginPage;

@RunWith(Parameterized.class)
public class LoginTest {
	private static Logger logger = LogManager.getLogger(LoginTest.class);
	static DataFormatter formatter = new DataFormatter();
	WebDriver driver;
	private String username;
	private String password;
	private String expectedString;

	public LoginTest(String username, String password, String expectedString) {
		this.username = username;
		this.password = password;
		this.expectedString = expectedString;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getExpectedString() {
		return expectedString;
	}

	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
		this.driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
	}

	@Test
	public void loginTest() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.inputUsernamePassword(username, password);
		loginPage.dologin();
		if (!username.equalsIgnoreCase("locked_out_user")) {
			HomeProductPage homeProductPage = new HomeProductPage(driver);
			assertEquals(expectedString, homeProductPage.getLoginConformationMessage());
			logger.info("Login Test Passed with: username:" + username + ", password:" + password);

		} else {
			assertTrue(loginPage.getloginErrorMessage().contains(expectedString));
			logger.info("Login Test Passed with: username:" + username + ", password:" + password);
		}
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File(".//loginTestSS/" + username + "_LoginStatus.png"));
		} catch (IOException e) {
			logger.error(e.toString());
			e.printStackTrace();
		}

	}

	@SuppressWarnings("rawtypes")
	@Parameters
	public static Collection loginData() throws IOException {
		FileInputStream fileInputStream = new FileInputStream("src/test/resources/loginData.xlsx");
		@SuppressWarnings("resource")
		XSSFWorkbook wb1 = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = wb1.getSheet("Sheet1");
		int rowCount = sheet.getPhysicalNumberOfRows();
		XSSFRow row = sheet.getRow(0);
		int colCount = row.getLastCellNum();
		System.out.println(colCount);

		System.out.println(rowCount);
		Object[][] array = new Object[rowCount - 1][colCount];
		for (int i = 1; i < rowCount; i++) {
			row = sheet.getRow(i);
			String username = formatter.formatCellValue(row.getCell(0));
			String password = formatter.formatCellValue(row.getCell(1));
			String expectedString = formatter.formatCellValue(row.getCell(2));
			array[i - 1][0] = username;
			array[i - 1][1] = password;
			array[i - 1][2] = expectedString;
		}
		return Arrays.asList(array);
	}

	@After
	public void closeBrowser() {
		driver.close();
	}

}

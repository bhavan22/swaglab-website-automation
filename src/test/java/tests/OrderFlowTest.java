package tests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CheckOutPage;
import pages.HomeProductPage;
import pages.LoginPage;
import pages.OrderCompletePage;
import pages.OverviewPage;
import pages.YourCartPage;

@RunWith(Parameterized.class)
public class OrderFlowTest {
	private static Logger logger = LogManager.getLogger(OrderFlowTest.class);
	static DataFormatter formatter = new DataFormatter();
	WebDriver driver;
	private String orderNUmberString;
	private String orderQtyString;
	private String itemNameString;
	private String itemQtyString;
	private String userNameString;
	private String passwordString;
	private String shippingFirstName;
	private String shippingLastName;
	private String shippingZipcode;

	public OrderFlowTest(String orderName, String orderQty, String itemName, String itemQty, String username,
			String password, String shippingfname, String shippingLname, String shippingZipc) {
		this.orderNUmberString = orderName;
		this.orderQtyString = orderQty;
		this.itemNameString = itemName;
		this.itemQtyString = itemQty;
		this.userNameString = username;
		this.passwordString = password;
		this.shippingFirstName = shippingfname;
		this.shippingLastName = shippingLname;
		this.shippingZipcode = shippingZipc;
	}

	public String getOrderNUmberString() {
		return orderNUmberString;
	}

	public String getOrderQtyString() {
		return orderQtyString;
	}

	public String getItemNameString() {
		return itemNameString;
	}

	public String getItemQtyString() {
		return itemQtyString;
	}

	public String getUserNameString() {
		return userNameString;
	}

	public String getPasswordString() {
		return passwordString;
	}

	public String getShippingFirstName() {
		return shippingFirstName;
	}

	public String getShippingLastName() {
		return shippingLastName;
	}

	public String getShippingZipcode() {
		return shippingZipcode;
	}

	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
		this.driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
	}

	@Test
	public void orderFlowTest() {
		String expectedString = "THANK YOU FOR YOUR ORDER";
		int oQ = Integer.parseInt(orderQtyString);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.inputUsernamePassword(userNameString, passwordString);
		loginPage.dologin();
		HomeProductPage homeProductPage = new HomeProductPage(driver);

		if (oQ > 1) {
			String[] items = itemNameString.split(",");

			for (int i = 0; i < items.length; i++) {
				homeProductPage.selectItemAndAddtoCart(items[i]);

			}
			homeProductPage.clickOnCartButton();
		} else {
			homeProductPage.selectItemAndAddtoCart(itemNameString);
			homeProductPage.clickOnCartButton();
		}
		YourCartPage yourCartPage = new YourCartPage(driver);
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File(".//OrderFlowSS/" + orderNUmberString + "_Cart.png"));
		} catch (IOException e) {
			logger.error(e.toString());
			e.printStackTrace();
		}

		yourCartPage.proceedToCheckout();
		CheckOutPage checkOutPage = new CheckOutPage(driver);
		checkOutPage.fillTheCheckOutDetails(shippingFirstName, shippingLastName, shippingZipcode);
		screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File(".//OrderFlowSS/" + orderNUmberString + "_ShippingDetails.png"));
		} catch (IOException e) {
			logger.error(e.toString());
			e.printStackTrace();
		}
		checkOutPage.clickOnContinue();
		OverviewPage overviewPage = new OverviewPage(driver);
		overviewPage.clickOnFinishButton();
		// overviewPage.clickOnSideBarAndLogout();
		OrderCompletePage orderCompletePage = new OrderCompletePage(driver);
		screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File(".//OrderFlowSS/" + orderNUmberString + "_OrderCompletion.png"));
		} catch (IOException e) {
			logger.error(e.toString());
			e.printStackTrace();
		}
		if (!orderCompletePage.getOrderConformationMessage().equals(expectedString)) {
			logger.error(
					"Order Failed: Order No:\" + orderNUmberString + \", Order Qty:\" + orderQtyString + \", User:\"\n"
							+ "				+ userNameString");

		}
		assertEquals(expectedString, orderCompletePage.getOrderConformationMessage());
		logger.info("Order Completed: Order No:" + orderNUmberString + ", Order Qty:" + orderQtyString + ", User:"
				+ userNameString);

	}

	@SuppressWarnings("rawtypes")
	@Parameters
	public static Collection ordersData() throws IOException {
		FileInputStream fileInputStream = new FileInputStream("src/test/resources/orderData.xlsx");
		@SuppressWarnings("resource")
		XSSFWorkbook wb1 = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = wb1.getSheet("Sheet1");
		int rowCount = sheet.getPhysicalNumberOfRows();
		XSSFRow row = sheet.getRow(0);
		int colCount = row.getLastCellNum();
		// System.out.println(colCount);

		// System.out.println(rowCount);
		Object[][] array = new Object[rowCount - 1][colCount];
		for (int i = 1; i < rowCount; i++) {
			row = sheet.getRow(i);
			String orderNumber = formatter.formatCellValue(row.getCell(0));
			String orderQty = formatter.formatCellValue(row.getCell(1));
			String itemName = formatter.formatCellValue(row.getCell(2));
			String itemQty = formatter.formatCellValue(row.getCell(3));
			String username = formatter.formatCellValue(row.getCell(4));
			String password = formatter.formatCellValue(row.getCell(5));
			String sFnameString = formatter.formatCellValue(row.getCell(6));
			String sLnameString = formatter.formatCellValue(row.getCell(7));
			String sZipcodeString = formatter.formatCellValue(row.getCell(8));

			array[i - 1][0] = orderNumber;
			array[i - 1][1] = orderQty;
			array[i - 1][2] = itemName;
			array[i - 1][3] = itemQty;
			array[i - 1][4] = username;
			array[i - 1][5] = password;
			array[i - 1][6] = sFnameString;
			array[i - 1][7] = sLnameString;
			array[i - 1][8] = sZipcodeString;

		}

		return Arrays.asList(array);
	}

	@After
	public void closeBrowser() {
		driver.close();
	}

}
